pub struct Goose {
    name: String
}

impl Goose {
    pub fn new(name: &str) -> Goose {
        return Goose { name: name.to_string() };
    }

    pub fn honk(&self) -> String {
        return format!("{}: {}", self.name, "Honk".to_string());
    }

    pub fn get_name(&self) -> String {
        return self.name.clone();
    }
}
