use duck::{behaviour::Quackable, DuckCall, MallardDuck, RedheadDuck, RubberDuck};
use goose::{Goose};

pub mod duck;
pub mod goose;

fn main() {
    let duck_call: Box<dyn Quackable> = Box::new(DuckCall::new("Bambang"));
    let mallard: Box<dyn Quackable> = Box::new(MallardDuck::new("Choco"));
    let redhead: Box<dyn Quackable> = Box::new(RedheadDuck::new("Dodo"));
    let rubber: Box<dyn Quackable> = Box::new(RubberDuck::new("Eddy"));

    simulate(&duck_call);
    simulate(&duck_call);
    simulate(&mallard);
    simulate(&mallard);
    simulate(&redhead);
    simulate(&redhead);
    simulate(&rubber);
    simulate(&rubber);

    // Goose can't use simulate
    let goose: Box<Goose> = Box::new(Goose::new("Freya"));
    println!("{}", goose.honk());
    println!("{}", goose.honk());
}

fn simulate(duck: &Box<dyn Quackable>) {
    println!("{}", duck.quack());
}
