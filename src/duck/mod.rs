pub mod behaviour;

use crate::duck::behaviour::Quackable;

pub struct DuckCall {
    name: String
}

impl Quackable for DuckCall {
    fn quack(&self) -> String {
        return format!("{}: {}", self.name, "Kwek".to_string());
    }
}

impl DuckCall {
    pub fn new(name: &str) -> DuckCall {
        return DuckCall { name: name.to_string() };
    }

    pub fn get_name(&self) -> String {
        return self.name.clone();
    }
}

pub struct MallardDuck {
    name: String
}

impl Quackable for MallardDuck {
    fn quack(&self) -> String {
        return format!("{}: {}", self.name, "Quack".to_string());
    }
}

impl MallardDuck {
    pub fn new(name: &str) -> MallardDuck {
        return MallardDuck { name: name.to_string() };
    }

    pub fn get_name(&self) -> String {
        return self.name.clone();
    }
}

pub struct RedheadDuck {
    name: String
}

impl Quackable for RedheadDuck {
    fn quack(&self) -> String {
        return format!("{}: {}", self.name, "Quack".to_string());
    }
}

impl RedheadDuck {
    pub fn new(name: &str) -> RedheadDuck {
        return RedheadDuck { name: name.to_string() };
    }

    pub fn get_name(&self) -> String {
        return self.name.clone();
    }
}

pub struct RubberDuck {
    name: String
}

impl Quackable for RubberDuck {
    fn quack(&self) -> String {
        return format!("{}: {}", self.name, "Squeak".to_string());
    }
}

impl RubberDuck {
    pub fn new(name: &str) -> RubberDuck {
        return RubberDuck { name: name.to_string() };
    }

    pub fn get_name(&self) -> String {
        return self.name.clone();
    }
}
