pub trait Quackable {
    fn quack(&self) -> String;
}
