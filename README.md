# Design Patterns that Work Together - Rust Version

This code example is based on Head First Design Patterns book published by O'Reilly Media.

---

## How to Run

This is just a simple repo made using Cargo.

Run with:
```bash
cargo run
```

You can check for compile errors without running by using this command:
```bash
cargo check
```
